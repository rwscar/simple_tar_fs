#include "File.hpp"
#include <vector>
#include <cassert>

using namespace std;

File::File(const string & _filename, MODE _mode):
  m_begin(0),
  m_end(0),
  m_position(0)
{
  const char * access = _mode == READ ? "rb" : "wb";
  FILE * f = fopen(_filename.c_str(), access);
  if (f)
  {
    fseek(f, 0, SEEK_END);
    SizeType size = ftell(f);
    rewind(f);
    m_file = shared_ptr<FILE>(f, &::fclose);
    m_end = m_begin + size;
  }
}

File::File(const File & _file, File::SizeType _offset, File::SizeType _size):
  m_file(_file.m_file),
  m_begin(_offset),
  m_end(_offset + _size),
  m_position(0)
{
  if (_offset + _size > _file.Size())
    Close();
}

File::~File()
{
  Close();
}

bool File::IsOpen() const
{
  return static_cast<bool>(m_file);
}

void File::Close()
{
  m_file = shared_ptr<FILE>();
  m_begin = 0;
  m_end = 0;
  m_position = 0;
}

File::SizeType File::Size() const
{
  return m_end - m_begin;
}

File::SizeType File::Tell() const
{
  return m_position; //m_offset + ftell(m_file.get());
}

File::SizeType File::Seek(File::SizeType _offset, File::DIRECTION _direction)
{
  SizeType newPosition = 0;
  switch (_direction)
  {
    case FROM_START:
      newPosition = min(m_begin + _offset, m_end);
      break;
    case FROM_CURRENT:
      newPosition = min(m_position + _offset, m_end);
      break;
    case FROM_END:
      newPosition = max(m_end - _offset, m_begin);
      break;
  }
  if (newPosition != m_position && fseek(m_file.get(), newPosition, SEEK_SET) == 0)
    m_position = newPosition;
  return m_position;
}

File::SizeType File::Read(void * _block, File::SizeType _size)
{
  if (Seek(m_position, FROM_START) != m_position)
    return 0;
  SizeType readBytes = fread(_block, 1, _size, m_file.get());
  m_position += readBytes;
  return readBytes;
}

File::SizeType File::Write(const void * _block, File::SizeType _size)
{
  if (Seek(m_position, FROM_START) != m_position)
    return 0;
  SizeType writtenBytes = fwrite(_block, 1, _size, m_file.get());
  m_position += writtenBytes;
  return writtenBytes;
}
