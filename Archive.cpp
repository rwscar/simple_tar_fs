#include "Archive.hpp"

using namespace std;

namespace
{

const uint64_t BLOCK_SIZE = 512;

}

struct RawFileHeader
{
  char name[100];
  char mode[8];
  char uid[8];
  char gid[8];
  char size[12];
  char mtime[12];
  char chksum[8];
  char typeflag;
  char linkname[100];
  // new
  char magic[6]; // ustar
  char version[2];
  char uname[32];
  char gname[32];
  char devmajor[8];
  char devminor[8];
  char prefix[155];
};

bool Archive::LoadFromFile(std::shared_ptr<File> _file)
{
  if (!_file)
    return false;

  int emptyBlocks = 0;
  char block[BLOCK_SIZE];
  unordered_map<string, pair<File::SizeType, File::SizeType>> files;

  while (_file->Read(block, BLOCK_SIZE) == BLOCK_SIZE && emptyBlocks < 2)
  {
    if (block[0] == '\0')
    {
      ++emptyBlocks;
      continue;
    }

    // read header
    auto rawHeader = reinterpret_cast<RawFileHeader *>(block);
    File::SizeType size = strtoull(rawHeader->size, nullptr, 8);
    File::SizeType offset = _file->Tell();
    if (static_cast<FILE_TYPE>(rawHeader->typeflag) == FILE_TYPE::NORMAL_FILE)
      files[string(rawHeader->name)] = make_pair(offset, size);

    File::SizeType skipBytesCount = size + (size % BLOCK_SIZE == 0 ? 0 : BLOCK_SIZE - size % BLOCK_SIZE);
    _file->Seek(skipBytesCount, File::FROM_CURRENT);
  };

  m_file = _file;
  m_files = move(files);

  printf("%-40s : %-10s: %-10s\n", "filename", "offset", "size");
  for (auto & x : m_files)
    printf("%-40s : %-10d: %-10d\n", x.first.c_str(), x.second.first, x.second.second);

  return true;
}

void Archive::Invalidate()
{
  m_file = shared_ptr<File>();
  m_files.clear();
}

std::shared_ptr<File> Archive::OpenFile(const std::string & _filename)
{
  if (!m_file)
    return nullptr;
  auto it = m_files.find(_filename);
  if (it == m_files.end())
    return nullptr;
  return make_shared<File>(*m_file, it->second.first, it->second.second);
}
