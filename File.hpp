#pragma once
#include <cstdint>
#include <string>
#include <memory>

class File
{
public:
  typedef uint64_t SizeType;

  enum MODE
  {
    READ,
    WRITE
  };

  enum DIRECTION
  {
    FROM_START,
    FROM_CURRENT,
    FROM_END
  };

  File(const std::string & _filename, MODE _mode = File::READ);
  File(const File & _file, SizeType _offset, SizeType _size);
  ~File();

  bool IsOpen() const;
  void Close();

  SizeType Size() const;

  SizeType Tell() const;
  SizeType Seek(SizeType _offset, DIRECTION _direction);

  SizeType Read(void * _block, SizeType _size);
  SizeType Write(const void * _block, SizeType _size);

private:
  std::shared_ptr<FILE> m_file;
  MODE                  m_mode;
  SizeType              m_begin;
  SizeType              m_end;
  SizeType              m_position;
};
