#pragma once
#include "File.hpp"
#include <cstdio>
#include <memory>
#include <unordered_map>

enum class FILE_TYPE : char
{
  NORMAL_FILE = '0',
  HARD_LINK = '1',
  SYMBOLIC_LINK = '2',
  CHARACTER_SPECIAL = '3',
  BLOCK_SPECIAL = '4',
  DIRECTORY = '5',
  FIFO = '6',
  CONTIGUOUS_FILE = '7'
};

struct FileInfo
{
  uint64_t offset;
  uint64_t size;
  FILE_TYPE type;
};

class Archive
{
public:
  bool LoadFromFile(std::shared_ptr<File> _file);
  void Invalidate();

  std::shared_ptr<File> OpenFile(const std::string & _filename);

private:

private:
  std::shared_ptr<File> m_file;
  std::unordered_map<std::string, std::pair<File::SizeType, File::SizeType>> m_files;
};
